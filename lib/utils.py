import json, os, boto3, subprocess
from jinja2 import Environment, FileSystemLoader

class cd:
    def __init__(self, path):
        self.path = os.path.expanduser(path)
        self.saved_path = ""
    
    def __enter__(self):
        self.saved_path = os.getcwdb()
        os.chdir(self.path)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.saved_path)


def load_configuration():
    try:
        with open('./conf/config.json') as config:
            return json.load(config)
    except Exception as er:
        raise er

def boto3_session(role_arn, region, profile=None):
    try:
        if profile :
            return boto3.Session(profile_name=profile, region_name=region)
        client = boto3.client('sts')
        response = client.assume_role(
            RoleArn=role_arn, RoleSessionName=f'tmp-{region}'
        )
        return boto3.Session(
            aws_access_key_id=response['Credentials']['AccessKeyId'],
            aws_secret_access_key=response['Credentials']['SecretAccessKey'],
            aws_session_token=response['Credentials']['SessionToken']
        )
    except Exception as e:
        raise e

def run(command, message, provide=None, env={}, stdout=False, max_retries=3):
    try:
        if stdout:
            print(message)
            res = subprocess.run(command.split(" "), env={**os.environ, **env}, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        else:
            print(message)
            res = subprocess.run(command.split(" "), env={**os.environ, **env})
        if res.returncode==0:
            if stdout:
                return res.stdout
        elif max_retries > 0:
            run(command, message, provide, env, stdout, max_retries-1)    
    except Exception as e:
        raise e

def patch_custodian():
    subprocess.call(". ./patch.sh", shell=True)

def find_all_rules(directory):
    for top, dirs, files in os.walk(directory):
        for file in files:
            if ".yml" in file:
                yield os.path.join(top, file)

def generate_tf_context(region, account_id, config):
    jinja2_env = Environment(loader=FileSystemLoader("./lib/tf_templates/"), autoescape=True)
    provider_template = jinja2_env.get_template('provider.tf.j2')
    provider_rendered = provider_template.render(
        region=region,
        profile=config['profile'],
        provider_version=config['provider_version']
    )
    with open("./terraform/00-base/provider.tf", "w") as outf:
        outf.write(provider_rendered)

    variables_template = jinja2_env.get_template('variables.tf.j2')
    variables_rendered = variables_template.render(
        iam_account_id=config['iam_account_id']
    )
    with open("./terraform/00-base/variables.tf", "w") as outf:
        outf.write(variables_rendered)


def deploy_terraform_layer(account_id, region, layer_name, config):
    print(f"Deploying terraform stack")
    generate_tf_context(region, account_id, config)
    with cd(f"./terraform/{layer_name}/"):
        run(f"terraform init", message="tf init")
        run(f"terraform apply --auto-approve", message="tf apply")

def remove_terraform_layer(account_id, region, layer_name, config):
    print(f"removing terraform stack")
    generate_tf_context(region, account_id, config)
    with cd(f"./terraform/{layer_name}/"):
        run(f"terraform init", message="tf init")
        run(f"terraform destroy --auto-approve", message="tf apply")


def deploy_custodian_rules(region, config, type):
    rules = find_all_rules(f"./rules/{type}")
    for rule in rules:
        run(f"custodian run {rule} --profile {config['profile']} --region {region} -s custodian-logs", message=f"Deploying Custodian rule {rule} on region {region}")

def clean_custodian_resources(account_id, config):
    if config['registration']['region'] not in config['regions_to_protect']:
        config['regions_to_protect'].append(config['registration']['region'])
    for region in config['regions_to_protect']:
        session = boto3_session("", region, config['profile'])
        lambda_client = session.client('lambda')
        for lambda_infos in lambda_client.list_functions()['Functions']:
            if lambda_infos['FunctionName'].startswith('SEC-c7n-'):
                print(f"Deleting function {lambda_infos['FunctionName']}")
                lambda_client.delete_function(FunctionName=lambda_infos['FunctionName'])
        logs_client = session.client('logs')
        for logs_infos in logs_client.describe_log_groups(logGroupNamePrefix='/aws/lambda/SEC-c7n-')['logGroups']:
            print(f"Deleting Log group {logs_infos['logGroupName']}")
            logs_client.delete_log_group(logGroupName=logs_infos['logGroupName'])
        events_client = session.client('events')
        for events_infos in events_client.list_rules(NamePrefix='SEC-c7n-')['Rules']:
            print(f"Deleting cloudwatch rule {events_infos['Name']}")
            resp = events_client.list_targets_by_rule(Rule=events_infos['Name'])['Targets']
            events_client.remove_targets(Rule=events_infos['Name'], Ids=[id['Id'] for id in resp])
            events_client.delete_rule(Name=events_infos['Name'])