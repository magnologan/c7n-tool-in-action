import argparse
from lib.utils import load_configuration, deploy_terraform_layer, deploy_custodian_rules, patch_custodian


def init(account_id):
    config = load_configuration()
    patch_custodian()

    deploy_terraform_layer(account_id, config['registration']['region'], '00-base', config)

    deploy_custodian_rules("us-east-1", config, type="global")

    for region in config["regions_to_protect"]:
        deploy_custodian_rules(region, config, type="regional")



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Will init the environnement")
    parser.add_argument("--account_id", required=True)
    parsed = parser.parse_args()
    init(parsed.account_id)