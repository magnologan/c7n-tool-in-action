resource "aws_iam_role" "UserRole" {
    name="SEC-PowerUser"
    path="/SECURITY/"

    permission_boundary=aws_iam_policy.UserRolePolicyBoundary.arn

    assume_role_policy = <<-EOF
    {
        "Version": "2012-10-17",
        "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
            "AWS": ${var.iam_account_id}
            },
            "Effect": "Allow"
        }
        ]
    }
    EOF
}

resource "aws_iam_role_policy" "UserRolePolicy" {
    name = "SEC-PowerUser-Policy"
    role = aws_iam_role.UserRole.id

    policy = <<-EOF
    {
        "Version": "2012-10-17",
        "Statement": [
        {
            "Action": [
            "iam:*"
            ],
            "Effect": "Allow",
            "Resource": "*"
        }
        ]
    }
    EOF
}

resource "aws_iam_policy" "UserRolePolicyBoundary" {
    name = "SEC-PowerUser-Policy-Boundary"

    policy = <<-EOF
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "1",
                "Effect": "Allow",
                "Action": [
                    "iam:CreateRole",
                    "iam:AttachRolePolicy",
                    "iam:DetachRolePolicy",
                    "iam:UpdateAssumeRolePolicy"
                ],
                "Resource": "*",
                "Condition": {
                    "StringEquals": {
                        "iam:PermissionBoundary": [
                            "arn:aws:iam::*:policy/SEC-PowerUser-Policy-Boundary"
                        ]
                    }
                }
            },
            {
                "Sid": "2",
                "Effect": "Allow",
                "Action": [
                    "iam:CreatePolicy",
                    "iam:DeletePolicy"
                ],
                "Resource": "*"
            }
        ]
    }
    EOF
}

