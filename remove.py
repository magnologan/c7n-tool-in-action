import argparse
from lib.utils import load_configuration, remove_terraform_layer, clean_custodian_resources


def remove(account_id):
    config = load_configuration()
    remove_terraform_layer(account_id, config['registration']['region'], '00-base', config)
    clean_custodian_resources(account_id, config)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Will init the environnement")
    parser.add_argument("--account_id", required=True)
    parsed = parser.parse_args()
    remove(parsed.account_id)