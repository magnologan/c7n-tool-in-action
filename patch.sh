folder="."

echo "Patching custodian"
to_patch="${folder}/env/lib/python3.7/site-packages/c7n/actions/notify.py"
patched="${folder}/lib/custodian-patchs/notify.py"
patch --no-backup-if-mismatch -u -s -N --verbose ${to_patch} -i ${patched}